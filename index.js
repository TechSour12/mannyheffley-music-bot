const Discord = require('discord.js');
const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES", "GUILD_VOICE_STATES"] });
const DisTube = require('distube');
const distube = new DisTube.default(client, { searchSongs: 5, emitNewSongOnly: true });
const DisTubeVoiceManager = new DisTube.DisTubeVoiceManager();
const { token } = require('./info.json'); // Make a file called info.json and put your bot token there
const prefix = 'f!';

function getQuote() {
    var responses = require('./quotes.json');
    var result = responses[Math.floor(Math.random() * responses.length)]
    try {
        messageCreate.channel.send(result).then();
    } catch (error) {
        return messageCreate.channel.send(`Error: ${error.stack}`)};
}

client.on("ready", () => {
    console.log(`${client.user.tag} logged in`)
});

client.on("messageCreate", async (messageCreate) => {
    if (!messageCreate.content.startsWith(prefix) || messageCreate.author.bot) return;
    const args = messageCreate.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    console.log(command);
    
    if (command === "play") {
        if (!messageCreate.member.voice.channel) return messageCreate.channel.send('Join a voice channel before trying to play something, stupid ploopy.');
        if (!args[0]) return messageCreate.channel.send('Tell me something to play, idiot.');
        DisTubeVoiceManager.join(messageCreate.member.voice.channel);
        distube.play(messageCreate, args.join(' ')); 
    }

    if (command === "stop") {
        const bot = messageCreate.guild.me;
        if (!messageCreate.member.voice.channel) return messageCreate.channel.send('Join a voice channel before trying to stop something, stupid ploopy.');
        if (bot.voice.channel !== messageCreate.member.voice.channel) return messageCreate.channel.send('Join **my** voice channel before trying to stop something, stupid ploopy.');
        distube.stop(messageCreate);
        messageCreate.channel.send(`> ${messageCreate.member.user.tag} has stopped the music.`);
    }

    if (command === "skip") {
        distube.skip(messageCreate);
        messageCreate.channel.send(`> ${messageCreate.member.user.tag} has skipped the current song.`);
    }

    if (command === "loop") {
        let mode = distube.setRepeatMode(message, parseInt(args[0]));
        mode = mode ? mode == 2 ? "Repeat queue" : "Repeat song" : "Off";
        messageCreate.channel.send("Set repeat mode to `" + mode + "`");
    }

    if (command === "queue") {
        let queue = distube.getQueue(messageCreate);
        messageCreate.channel.send("> Current queue:\n" + queue.songs.map((song, id) =>
            `**${id+1}**. [${song.name}](${song.url}) - \`${song.formattedDuration}\``
        ).join("\n"));
    }

    if (command === "seek") {
        distube.seek(messageCreate, Number(args[0]));
        messageCreate.channel.send(`> ${messageCreate.member.user.tag} has moved the video playback to ${messageCreate, Number(args[0])}.`)
    }

    if (command === "volume") {
        distube.setVolume(messageCreate, args[0]);
        messageCreate.channel.send(`> ${messageCreate.member.user.tag} has set the volume to ${messageCreate, args[0]}.`)
    }

    if (command === "jump") {
        distube.jump(messageCreate, parseInt(args[0]))
            .catch(err => { messageCreate.channel.send("Not a valid song number in the queue."); return;});
            messageCreate.channel.send(`> Jumped to **${messageCreate, parseInt(args[0])}** in the queue. .`);
    }  

    if (command === "resume") {
        distube.resume(messageCreate);
        messageCreate.channel.send(`> ${messageCreate.member.user.tag} has resumed the music.`)
    }

    if (command === "shuffle") {
        distube.shuffle(messageCreate);
        messageCreate.channel.send(`> ${messageCreate.member.user.tag} has shuffled the queue music.`)
    }

    if (command === "help") {
        messageCreate.channel.send(`> \`!play\` / \`!p\` - Play a song\n\`!stop\` / \`!s\` - Stop a song\n\`!resume\` / \`!r\` - Resume a song\n\`!skip\` / \`!sk\` - Skip a song\n\`!loop 1-2-3\` / \`!lo 1-2-3\` - Loop the queue or a song\n\`!queue\` / \`!q\` - View the queue\n\`!seek\` / \`!se\` - Jump to a part of a song\n\`!volume\` / \`!v\` - Adjust the bot volume\n\`!shuffle\` / \`!sh\` - Shuffle the songs in the queue\n\`!join\` / \`!j\` - Join the voice channel\n\`!disconnect\` / \`!ds\` - Leave the voice channel`)
    }

    if (messageCreate.content === '+ping') {  
        getQuote();
      }

    if (command === "join") {
        if(!messageCreate.member.voice.channel) return messageCreate.channel.send('Join a voice channel before asking me to join, ploopy.');
        DisTubeVoiceManager.join(messageCreate.member.voice.channel);
        messageCreate.channel.send('Joined the voice channel.');
    }

    if (command === "disconnect") {
        DisTubeVoiceManager.leave(messageCreate.member.voice.channel);
        messageCreate.channel.send('Left the voice channel.');
    } 

    
});

const status = (queue) => `Volume: \`${queue.volume}%\` | | Loop: \`${queue.repeatMode ? queue.repeatMode == 2 ? "All Queue" : "This Song" : "Off"}\` | Autoplay: \`${queue.autoplay ? "On" : "Off"}\``;

distube
    .on("playSong", (messageCreate, queue, song) => {
        console.log("play song");
        if (song.playlist) {
            return messageCreate.channel.send(`> Playing the playlist **${playlist.name}** (${playlist.songs.length} songs).\n${song.user.tag}\n\nNow playing \`${song.name}\` - ( \`${song.formattedDuration}\` )\n${status(queue)}`);
        } 
        else {
            return messageCreate.channel.send(`> Playing **${song.name}** - ( \`${song.formattedDuration}\` )\${song.user.tag}\n${status(queue)}`);
        }
    })

    .on("addSong", (messageCreate, queue, song) => {
        console.log("add song");
        console.log(song);
        if (song.playlist) {
            return messageCreate.channel.send(`> Added the playlist **${playlist.name}** (${playlist.songs.length} songs) to the queue\n${status(queue)}`);
        }
        else {
            return messageCreate.channel.send(`> Added **${song.name}** - ( \`${song.formattedDuration}\` ) to the queue\n${song.user.tag}`);
        }
    })
        
    .on("searchResult", (messageCreate, result) => {
        let i = 0;
        return messageCreate.channel.send(`**Choose a song from below:**\n${result.map(song => `**${++i}**: ${song.name} - ( \`${song.formattedDuration}\` )`).join("\n")}\n*Enter anything else or wait 60 seconds to abort the search.*`);
    })

    .on("searchNoResult", (messageCreate, result) => {
        return messageCreate.channel.send(`No results found. Maybe be less specific, ploopy.`);
    })

    .on("searchInvalidAnswer", (messageCreate, result) => {
        return messageCreate.channel.send(`Invalid answer, ploopy.`);
    })

    .on("searchDone", (messageCreate, result) => {
        return messageCreate.channel.send(`Done.`);
    })
        
    .on("searchCancel", (messageCreate) => messageCreate.channel.send(`Search aborted.`))
    .on("error", (messageCreate, e) => {
        return console.error(e);
     }); 
    
client.login(token);
